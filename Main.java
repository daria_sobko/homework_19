
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Math.*;


public class Main {
    WebDriver driver;


    @BeforeClass
    public static void setup (){
        System.setProperty("webdriver.gecko.driver", "/Users/darasobko/Documents/gecko/geckodriver");
    }

    @Before
    public void preCondition(){
        driver = new FirefoxDriver();
    }

    @Test
    public void registration() {
        /*Регистрация случайного пользователя*/
        String randomEmail = "mail" + round(random() * 10000) + "@gmail.com";
        String randomPhone = "3806723" + ((int)floor(random() * 100000));
    driver.get("http://user-data.hillel.it/html/registration.html");
    driver.findElement(By.className("registration")).click();
    driver.findElement(By.id("first_name")).sendKeys("Test");
    driver.findElement(By.id("last_name")).sendKeys("Dasha");
    driver.findElement(By.id("field_work_phone")).sendKeys("1234567");
    driver.findElement(By.id("field_phone")).sendKeys(randomPhone);
    driver.findElement(By.id("field_email")).sendKeys(randomEmail);
    driver.findElement(By.id("field_password")).sendKeys("aB123456");
    driver.findElement(By.id("female")).click();
    Select dropdown = new Select(driver.findElement(By.id("position")));
    dropdown.selectByVisibleText("qa");
    driver.findElement(By.id("button_account")).click();
    Alert alert = new WebDriverWait(driver,20).
            until(ExpectedConditions.alertIsPresent());
    alert.accept();
    /*Авторизация*/
    driver.navigate().refresh();
    driver.findElement(By.id("email")).sendKeys(randomEmail);
    driver.findElement(By.id("password")).sendKeys("aB123456");
    driver.findElement(By.className("login_button")).click();
    /*Поиск и вывод данных*/
    driver.findElement(By.id("employees")).click();
    driver.findElement(By.id("first_name")).sendKeys("Test");
    driver.findElement(By.id("last_name")).sendKeys("Dasha");
    driver.findElement(By.id("mobile_phone")).sendKeys(randomPhone);
    driver.findElement(By.id("search")).click();
    List <WebElement> td = driver.findElements(By.tagName("td"));
        System.out.println(td.size());
        for (int i =0; i < td.size(); i++ ){
            System.out.println(td.get(i).getText());
        }
    }

    @After
    public void postCondition(){
        driver.quit();
    }

}
